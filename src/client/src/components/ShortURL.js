import React from 'react';

class ShortURL extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            shorturl: props.shorturl,
            longurl: props.longurl,
        }
        this.copytoClipboard = this.copytoClipboard.bind(this);
    }

    copytoClipboard(e) {
        this.shortURLref.select();
        document.execCommand('copy');
        e.target.focus()
    }

    render() {
        return (
            <div class="short-url mdl-card mdl-shadow--3dp">
                <p>Original URL: <a href={this.state.longurl}>{this.state.longurl}</a></p>
                <p>
                    Short URL: <input class="short-url-link" ref={(input) => this.shortURLref = input} value={this.state.shorturl}/> <a href={this.state.shorturl}>Go To Link</a>
                </p>
                <button onClick={this.copytoClipboard} class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-shadow--4dp">
                    Copy Short URL
                </button>
            </div>
        );
    }

}

export default ShortURL;