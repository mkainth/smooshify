import React from 'react';

class URLAnalysis extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            urlData: props.data,
        }
    }

    render() {

        var HistoryTable = null;
        if (this.props.data.people !== null) {
            HistoryTable = (
                <>
                    <h5>URL Visitors</h5>
                    <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
                    <thead>
                        <tr>
                        <th class="mdl-data-table__cell--non-numeric">IP Address</th>
                        <th class="mdl-data-table__cell--non-numeric">Time (UTC)</th>
                        </tr>
                    </thead>
                    <tbody>

                    {
                        this.props.data.people.map(person => (
                            <tr>
                            <td class="mdl-data-table__cell--non-numeric">{person.ip}</td>
                            <td class="mdl-data-table__cell--non-numeric">{person.visittime}</td>
                            </tr>
                        ))
                    }

                    </tbody>
                    </table>
                </>
            );
        }

        return (
            <div class="short-url mdl-card mdl-shadow--3dp">
                <h3>Short URL Analytics</h3>
                <p>Original URL: <a href={this.props.data.longurl}>{this.props.data.longurl}</a></p>
                <p>Short URL: <a href={"http://smooshify.com/"+this.props.data.shortid}>{"http://smooshify.com/"+this.props.data.shortid}</a></p>
                <p>Number of Visits: {this.props.data.visits}</p>
                {HistoryTable}
            </div>
        );
    }
}

export default URLAnalysis;