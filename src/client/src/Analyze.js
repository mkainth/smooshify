import React from 'react';
import URLAnalysis from './components/URLAnalysis';

class Analyze extends React.Component {
   constructor(props){
        super(props);
        this.state = {
            textfield: "",
            urlData: null,
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleTextFieldChange = this.handleTextFieldChange.bind(this);
    }

    handleFormSubmit(e) {
        var textfieldArray = this.state.textfield.split("/");
        e.preventDefault();
        fetch('http://smooshify.com/url/' + textfieldArray[textfieldArray.length - 1], {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => response.json())
            .then(data => {
                this.setState({urlData: data});
                this.setState(this.state);
                console.log(this.state);
            });
    }

    handleTextFieldChange(e) {
        this.setState({textfield: e.target.value});
        console.log(this.state.textfield);
    }

    render() {
        var analyticComponent = null;
        if (this.state.urlData !== null) {
            analyticComponent = (<URLAnalysis data={this.state.urlData}/>)
        }
        
        return (
            <div>
                <form onSubmit={this.handleFormSubmit}>
                <div id="analyze-textfield" class="long-url-input mdl-textfield mdl-js-textfield mdl-textfield is-upgraded">
                    <label class="textfield-label">Enter a Short URL to Analyze</label>
                    <input class="mdl-textfield__input" type="text" value={this.state.textfield} onChange={this.handleTextFieldChange}/>
                </div>
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
                    Analyze
                </button>

                </form>
                {analyticComponent}
            </div>
        );
    }
}

export default Analyze;