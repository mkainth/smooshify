import React from 'react';
import ShortURL from './components/ShortURL';

class Home extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            textfield: "",
            urls: [],
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleTextFieldChange = this.handleTextFieldChange.bind(this);
    }

    handleFormSubmit(e) {
        e.preventDefault();
        fetch('http://smooshify.com/url', {
            method: "POST",
            body: JSON.stringify({"longurl": this.state.textfield}),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => response.json())
            .then(data => {
                var newURLState = this.state.urls.slice();
                newURLState.push(data);
                this.setState({urls: newURLState});
                console.log(this.state);
            });
    }

    handleTextFieldChange(e) {
        this.setState({textfield: e.target.value});
        console.log(this.state.textfield);
    }

    render() {
        const URLComponents = (
            <ul class="short-url-cards">
                {this.state.urls.map(url => (
                    <li class="short-url-ele"><ShortURL longurl={url.longurl} shorturl={url.shorturl}/></li>
                ))}
            </ul>
        );
        
        return (
            <div>
                <form onSubmit={this.handleFormSubmit}>
                    <div id="smoosh-textfield" class="long-url-input mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="textfield-label">Enter a URL to Smoosh</label>
                        <input class="mdl-textfield__input" type="text" value={this.state.textfield} onChange={this.handleTextFieldChange}/>
                    </div>
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
                        Smoosh
                    </button>
                </form>
                {URLComponents}
            </div>
        );
    }
}

export default Home;