import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import Home from './Home';
import Analyze from './Analyze';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Helmet>
          <meta name="mobile-web-app-capable" content="yes"/>
          <link rel="icon" sizes="192x192" href="images/android-desktop.png"></link>
          <meta name="apple-mobile-web-app-capable" content="yes"/>
          <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
          <meta name="apple-mobile-web-app-title" content="Material Design Lite"/>
          <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png"></link>
          <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png"/>
          <meta name="msapplication-TileColor" content="#3372DF"/>
          <link rel="shortcut icon" href="images/favicon.png"></link>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en"></link>
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>
          <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.deep_purple-pink.min.css"></link>
          <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
        </Helmet>
        <div class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">
          <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
              <div class="mdl-layout--large-screen-only mdl-layout__header-row">
              </div>
              <div class="mdl-layout--large-screen-only mdl-layout__header-row">
                <h3>Smooshify.com</h3>
              </div>
              <div class="mdl-layout--large-screen-only mdl-layout__header-row">
                <h5>A short-url service with analytics.</h5>
              </div>
              <div class="mdl-layout--large-screen-only mdl-layout__header-row">
              <a href="https://michaelkainth.com">
                <h6>By: Michael Kainth</h6>
              </a>
              </div>
              <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color--primary-dark">
                <Link to="/" class="mdl-layout__tab is-active">Home</Link>
                <Link to="/analyze" class="mdl-layout__tab">Analyze</Link>
              </div>
            </header>

            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/analyze/" component={Analyze} />
            </Switch>

            <a href="https://bitbucket.org/mkainth/smooshify/src/master/" target="_blank" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">View Source</a>
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
