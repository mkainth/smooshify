/*
Go server for Smooshify.com (short URL service)
Author: Michael Kainth
Date: May 18, 2019
*/

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
	"io/ioutil"
	"strings"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/rs/xid"
	"github.com/tomasen/realip"
)

var client *mongo.Client
var db *mongo.Database
var ctx context.Context

// this struct contains the data collected when a person visits a URL
type Person struct {
  IP            string          `json:"ip" bson:"ip"`
  VisitTime		string        	`json:"visittime" bson:"visittime"`
}

// every short url will have a mapping like this in the db
// this contains all information pertaining to a short url
type URLMapping struct {
  ShortID   string              `json:"shortid" bson:"shortid"`
  LongURL   string              `json:"longurl" bson:"longurl"`
  CreatedOn string              `json:"createdon" bson:"createdon"`
  People    []Person            `json:"people" bson:"people"`
  Visits    int                 `json:"visits" bson:"visits"`
}

type ShortURLRequest struct {
	LongURL 	string			`json:"longurl" bson:"longurl"`
}

type URLDataRequest struct {
	ShortURL	string			`json:"shorturl" bson:"shorturl"`
}

type ShortURLResponse struct {
	ShortURL	string			`json:"shorturl" bson:"shorturl"`
	LongURL	  	string			`json:"longurl" bson:"longurl"`
}

type ErrorJSON struct {
	Error		string			`json:"error" bson:"error"`
}

// timestamps will be formated in ISO 1806
func getTimestamp() string {
  return time.Now().UTC().Format("2006-01-02T15:04:05-0700")
}

// creates a new short url entry in the database
// returns the response json
func createShortURL(longURL string) string {
	var guid xid.ID = xid.New()
		
	// generate unique id for small url using xid algorithm
	// source: https://blog.kowalczyk.info/article/JyRZ/generating-good-unique-ids-in-go.html
	var shortID string = guid.String()
	
	// add the http protocol to the url
	if !strings.Contains(longURL, "://") {
		longURL = "http://" + longURL
	}

	newShortURL := URLMapping{
		ShortID: shortID,
		LongURL: longURL,
		CreatedOn: getTimestamp(),
		People: []Person{},
		Visits: 0,
	}

	_, err := db.Collection("urlStore").InsertOne(nil, newShortURL)

	if err != nil {
		return errorResponse(err)
	}

	newURLResponse := ShortURLResponse{
		ShortURL: fmt.Sprintf("http://smooshify.com/%s", shortID),
		LongURL: longURL,
	}

	responseContent, err := json.Marshal(newURLResponse)

	if err != nil {
		return errorResponse(err)
	}

	return string(responseContent)

}

// enable Cross Origin Resource Sharing so we can make the API accessible by JavaScript
// also handles preflight options request (for POST requests)
func enableCors(response *http.ResponseWriter) {
	(*response).Header().Set("Access-Control-Allow-Origin", "*")
    (*response).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    (*response).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

// returns the error formatted as a JSON string
func errorResponse(err error) string {

	eJSON := ErrorJSON{
		Error: err.Error(),
	}

	responseContent, e := json.Marshal(eJSON)
	
	if e != nil {
		return "{'error': 'error'}"
	} else {
		return string(responseContent)
	}

}

// this handler generates a short url and responds with a json containing the short url and long url
func shortURLHandler(response http.ResponseWriter, request *http.Request) {
	enableCors(&response)
	switch request.Method {
	case "POST":

		reqBody, err := ioutil.ReadAll(request.Body)

		if err != nil {
			response.Write([]byte(errorResponse(err)))
			return
		}

		var requestData ShortURLRequest

		err = json.Unmarshal([]byte(reqBody), &requestData)

		if err != nil {
			response.Write([]byte(errorResponse(err)))
			return
		}

		response.Write([]byte(createShortURL(requestData.LongURL)))
  }
}

// this handler handles multiple cases:
// 		1. it acts as a file server to for static react pages
// 		2. it serves an endpoint for getting url analytics
//		3. is also handles short url redirects
func customRootHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		// remove the first character of the url path ("/")
		urlPath := request.URL.Path[1:]

		// handle request for url data (analytics)
		if strings.HasPrefix(urlPath, "url") {
			enableCors(&response)

			var urlData URLMapping
			err := db.Collection("urlStore").FindOne(nil, bson.M{"shortid": urlPath[4:]}).Decode(&urlData)

			responseContent, err := json.Marshal(urlData)
	
			if err != nil {
				urlData = URLMapping{
					ShortID: "ERROR: item not found",
					LongURL: "ERROR: item not found",
					CreatedOn: "ERROR: item not found",
					People: []Person{},
					Visits: 0,
				}
				responseContent, _ = json.Marshal(&urlData)
				response.Write([]byte(responseContent))
				return
			}
			response.Write([]byte(responseContent))
		
		// handles case for static react pages
		} else if urlPath == "" || strings.ContainsAny(urlPath, "./") {
			handler.ServeHTTP(response, request)
		
		// if the user is trying to acces the analyze react page, then redirect to homepage
		// In the react implementation, we did not implement a separate static page for analyze
		} else if strings.ToLower(urlPath) == "analyze" {
			http.Redirect(response, request, "/", http.StatusSeeOther)

		// handles short url redirects
		} else {
			var urlData URLMapping
			err := db.Collection("urlStore").FindOne(nil, bson.M{"shortid": urlPath}).Decode(&urlData)
			if err != nil {
				// if the query fails, redirect to the homepage
				http.Redirect(response, request, "/", http.StatusSeeOther)
				return
			}

			// this set of instructions is to increment the number of visits by 1, track ip address & time
			update := bson.D{
				{ "$inc", bson.D{ {"visits", 1} } },
				{"$push", bson.D{ {"people", bson.D{ {"ip", realip.FromRequest(request)}, {"visittime", getTimestamp()}, } }, } },
			}

			// if the url exists, then update the respective mongo document and redirect to the long URL
			if urlData.LongURL != "" {
				db.Collection("urlStore").UpdateOne(nil, bson.M{"shortid": urlPath}, update)
				http.Redirect(response, request, urlData.LongURL, http.StatusSeeOther)
			} else {
				// if the url doesn't exist, redirect to the homepage
				// TODO: add check for invalid urls
				http.Redirect(response, request, "/", http.StatusSeeOther)
			}
		}
	})
}

func main() {
	fmt.Println("Starting the application...")
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)

	// Establish connection with mongo db
	client, err := mongo.Connect(ctx, "connection string")
	
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(ctx, nil)

	if err != nil {
			log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	db = client.Database("smooshify")

	// This handler is a hybrid of a fileserver and http api
	// Used for:
	//		1. navigating to the homepage
	//		2. redirecting a short url to a long url
	//		3. obtaining url data for analytics 
	fileServerHandler := http.FileServer(http.Dir("./build"))
	http.Handle("/", customRootHandler(fileServerHandler))

	// JSON Service: Used for creating and managing new short URLs (returns a JSON with the short URL)
	http.HandleFunc("/url", shortURLHandler)

	err = http.ListenAndServe(":80", nil)
	fmt.Println(err)
}