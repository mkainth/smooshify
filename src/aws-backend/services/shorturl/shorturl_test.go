package shorturl

import (
	"fmt"
	"testing"
)

func TestCreateURL(t *testing.T) {
	originalURL, UserID, Origin, IP := "google.com/test", "", "", ""
	newURL, err := CreateURL(originalURL, UserID, Origin, IP)
	if err != nil {
		t.Error(err)
	}
	if len(newURL.ShortKey) <= 0 {
		t.Error("CreateURL failed to produce correct result.")
	}
	fmt.Printf("TestCreateURL() generated short-url value: %s\n", newURL.ShortKey)
}
