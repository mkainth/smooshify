package shorturl

import (
	"encoding/hex"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/capitalone/fpe/ff1"
)

type URL struct {
	Type        string `json:"pk"`
	ShortKey    string `json:"sk"`
	OriginalURL string `json:"original_url"`
	Timestamp   string `json:"timestamp"`
	UserID      string `json:"userid"`
	Origin      string `json:"origin"`
	IP          string `json:"ip"`
}

type URLResponse struct {
	ShortKey    string `json:"short_key"`
	OriginalURL string `json:"original_url"`
	Timestamp   string `json:"timestamp"`
	UserID      string `json:"userid"`
	Origin      string `json:"origin"`
	IP          string `json:"ip"`
}

type POSTURLBody struct {
	URL string `json:"url"`
}

type URLCounterInc struct {
	Increment int `json:":val"`
}

type URLCounterUpdated struct {
	Count int `json:"counter_value"`
}

type AtomicCounterKey struct {
	Type string `json:"pk"`
	Name string `json:"sk"`
}

func CreateURLResponse(url URL) *URLResponse {
	return &URLResponse{
		ShortKey:    url.ShortKey,
		OriginalURL: url.OriginalURL,
		Timestamp:   url.Timestamp,
		UserID:      url.UserID,
		Origin:      url.Origin,
		IP:          url.IP,
	}
}

func CreateURL(originalURL, UserID, Origin, IP string) (*URLResponse, error) {
	timestamp := time.Now().Format(time.RFC3339)
	urlCount, err := getAtomicURLCount()
	if err != nil {
		return &URLResponse{}, err
	}
	shortkey, err := generateShortKey(strconv.Itoa(urlCount))
	if err != nil {
		return &URLResponse{}, err
	}
	urlEntry := URL{
		Type:        "short_url",
		ShortKey:    shortkey,
		OriginalURL: originalURL,
		Timestamp:   timestamp,
		UserID:      UserID,
		Origin:      Origin,
		IP:          IP,
	}
	av, err := dynamodbattribute.MarshalMap(urlEntry)
	if err != nil {
		return &URLResponse{}, err
	}
	svc := dynamodb.New(session.New(&aws.Config{
		Region: aws.String("ca-central-1")},
	))
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("smooshify"),
	}
	if err != nil {
		return &URLResponse{}, err
	}
	_, err = svc.PutItem(input)
	if err != nil {
		return &URLResponse{}, err
	}
	return &URLResponse{
		ShortKey:    shortkey,
		OriginalURL: originalURL,
		Timestamp:   timestamp,
		UserID:      UserID,
		Origin:      Origin,
		IP:          IP,
	}, nil
}

func getAtomicURLCount() (int, error) {
	svc := dynamodb.New(session.New(&aws.Config{
		Region: aws.String("ca-central-1")},
	))
	key, err := dynamodbattribute.MarshalMap(AtomicCounterKey{
		Type: "atomic_counter",
		Name: "url_counter",
	})
	if err != nil {
		return 0, err
	}
	increment, err := dynamodbattribute.MarshalMap(URLCounterInc{
		Increment: 1,
	})
	if err != nil {
		log.Println(err)
		return 0, err
	}
	input := &dynamodb.UpdateItemInput{
		Key:                       key,
		TableName:                 aws.String("smooshify"),
		UpdateExpression:          aws.String("set counter_value = counter_value + :val"),
		ExpressionAttributeValues: increment,
		ReturnValues:              aws.String("UPDATED_NEW"),
	}
	result, err := svc.UpdateItem(input)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	updatedAttributes := URLCounterUpdated{}
	err = dynamodbattribute.UnmarshalMap(result.Attributes, &updatedAttributes)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	return updatedAttributes.Count, nil
}

func generateShortKey(globalcount string) (string, error) {
	// Key and tweak should be byte arrays. Put your key and tweak here.
	// To make it easier for demo purposes, decode from a hex string here.
	key, err := hex.DecodeString(os.Getenv("ff1_key"))
	if err != nil {
		return "", err
	}
	tweak, err := hex.DecodeString(os.Getenv("ff1_tweak"))
	if err != nil {
		return "", err
	}
	// Create a new FF1 cipher "object"
	// 10 is the radix/base, and 8 is the tweak length.
	FF1, err := ff1.NewCipher(36, 8, key, tweak)
	if err != nil {
		return "", err
	}
	original := globalcount
	// Call the encryption function on an example SSN
	ciphertext, err := FF1.Encrypt(original)
	if err != nil {
		return "", err
	}
	return ciphertext, nil
}

func GetURL(shortKey string) (*URLResponse, error) {
	svc := dynamodb.New(session.New(&aws.Config{
		Region: aws.String("ca-central-1")},
	))
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("smooshify"),
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String("short_url"),
			},
			"sk": {
				S: aws.String(shortKey),
			},
		},
	})
	if err != nil {
		return &URLResponse{}, err
	}
	item := URL{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		return &URLResponse{}, err
	}
	return CreateURLResponse(item), nil
}

// make event entry into ElasticSearch
/* Elastic Search Event Data Structure
{
	Event_Type	 string `json:"event_type"`
	Short_Key    string `json:"short_key"`
	Timestamp    string `json:"timestamp"`
	Origin       string `json:"origin"`
	IP           string `json:"ip"`
	GeoHash		 string `json:"geohash"`
}
*/
