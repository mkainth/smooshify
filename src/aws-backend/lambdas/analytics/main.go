package main

import (
	"encoding/json"
	"errors"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

func HandleLambda(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var path []string
	for _, v := range strings.Split(request.Path, "/") {
		if v != "" {
			path = append(path, v)
		}
	}
	// path[0] is the stage
	switch path[1] {
	case "analytics":
		return APIError(errors.New("resource not yet implemented"), 501)
	}
	return APIError(errors.New("resource path does not exist"), 404)
}

func APIError(err error, statuscode int) (events.APIGatewayProxyResponse, error) {
	ErrorResponse := ErrorResponse{
		Error: err.Error(),
	}
	responseBody, err := json.Marshal(ErrorResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	return events.APIGatewayProxyResponse{Body: string(responseBody), StatusCode: statuscode}, nil
}

func main() {
	lambda.Start(HandleLambda)
}
