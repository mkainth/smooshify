/**********************************************
	URL microservice for creating tiny URLs
	Author: Michael Kainth
	Date: September 8, 2019
***********************************************/

package main

import (
	"encoding/json"
	"errors"
	"log"
	"strings"

	"github.com/aws/aws-lambda-go/events"

	"github.com/aws/aws-lambda-go/lambda"
	//url "bitbucket.org/mkainth/smooshify/src/aws-backend/services/shorturl"
	"bitbucket.org/mkainth/smooshify/src/aws-backend/services/shorturl"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

func HandleURL(request *events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var response []byte
	if request.HTTPMethod == "GET" {
		url, err := shorturl.GetURL(request.QueryStringParameters["key"])
		if err != nil {
			return APIError(err, 404)
		}
		response, err = json.Marshal(url)
		if err != nil {
			return APIError(err, 404)
		}
	} else if request.HTTPMethod == "POST" {
		body := []byte(request.Body)
		var PostURL shorturl.POSTURLBody
		err := json.Unmarshal(body, &PostURL)
		if err != nil {
			return APIError(err, 404)
		}
		url, err := shorturl.CreateURL(PostURL.URL, "mkainth", "Canada", "192.1.1.1")
		if err != nil {
			return APIError(err, 404)
		}
		response, err = json.Marshal(url)
		if err != nil {
			return APIError(err, 404)
		}
	} else {
		return APIError(errors.New(request.HTTPMethod+" HTTP method not allowed"), 405)
	}
	return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
}

func HandleURLRedirect(request *events.APIGatewayProxyRequest, key string) (events.APIGatewayProxyResponse, error) {
	var redirectURL string
	if request.HTTPMethod == "GET" {
		url, err := shorturl.GetURL(key)
		if err != nil {
			return APIError(err, 404)
		}
		redirectURL = url.OriginalURL
	} else {
		return APIError(errors.New(request.HTTPMethod+" HTTP method not allowed"), 405)
	}
	return events.APIGatewayProxyResponse{Headers: map[string]string{"Location": redirectURL}, StatusCode: 301}, nil
}

func LogRequest(request *events.APIGatewayProxyRequest) {
	log.Println("request.Resource ", request.Resource)                                               //                       string                        `json:"resource"` // The resource path defined in API Gateway
	log.Println("request.Path ", request.Path)                                                       //                            string                        `json:"path"`     // The url path for the caller
	log.Println("request.HTTPMethod ", request.HTTPMethod)                                           //                      string                        `json:"httpMethod"`
	log.Println("request.Headers ", request.Headers)                                                 //                         map[string]string             `json:"headers"`
	log.Println("request.MultiValueHeaders ", request.MultiValueHeaders)                             //              map[string][]string           `json:"multiValueHeaders"`
	log.Println("request.QueryStringParameters ", request.QueryStringParameters)                     //           map[string]string             `json:"queryStringParameters"`
	log.Println("request.MultiValueQueryStringParameters ", request.MultiValueQueryStringParameters) // map[string][]string           `json:"multiValueQueryStringParameters"`
	log.Println("request.PathParameters ", request.PathParameters)                                   //                  map[string]string             `json:"pathParameters"`
	log.Println("request.StageVariables ", request.StageVariables)                                   //                  map[string]string             `json:"stageVariables"`
	log.Println("request.RequestContext ", request.RequestContext)                                   //                  APIGatewayProxyRequestContext `json:"requestContext"`
	log.Println("request.Body ", request.Body)                                                       //                            string                        `json:"body"`
}

func HandleLambda(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	//LogRequest(&request)
	var path []string
	for _, v := range strings.Split(request.Path, "/") {
		if v != "" {
			path = append(path, v)
		}
	}
	// path[0] is the stage
	switch path[1] {
	case "url":
		return HandleURL(&request)
	case "link":
		return HandleURLRedirect(&request, path[2])
	}
	return APIError(errors.New("resource path does not exist"), 404)
}

func APIError(err error, statuscode int) (events.APIGatewayProxyResponse, error) {
	ErrorResponse := ErrorResponse{
		Error: err.Error(),
	}
	responseBody, err := json.Marshal(ErrorResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	return events.APIGatewayProxyResponse{Body: string(responseBody), StatusCode: statuscode}, nil
}

func main() {
	lambda.Start(HandleLambda)
}
