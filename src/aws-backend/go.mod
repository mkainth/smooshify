module bitbucket.org/mkainth/smooshify/src/aws-backend

go 1.13

require (
	github.com/aws/aws-lambda-go v1.13.2
	github.com/aws/aws-sdk-go v1.23.21
	github.com/capitalone/fpe v1.2.1
	github.com/pkg/errors v0.8.1 // indirect
	github.com/theckman/go-ipdata v0.5.0
)
