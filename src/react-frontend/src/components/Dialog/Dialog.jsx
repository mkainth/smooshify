import React from "react";
import { MDCDialog } from "@material/dialog";
import "./Dialog.scss"

class Dialog extends React.Component {
    constructor(props) {
        super(props);

        // Init state
        this.state = {
        };
    }

    componentDidMount() {
        this.dialog = new MDCDialog(this.refs.dialog);
        // provide control to parent component to open the dialog from there
        this.props.provideCtrl({
            show: () => this.dialog.show(),
            close: () => this.dialog.close()
        });
    }

    componentWillUnmount() {
        this.props.provideCtrl(null);
    }

    render() {
        return (
            <aside
                ref="dialog"
                id="test_dialog"
                className="mdc-dialog"
                role="alertdialog"
                aria-labelledby="test_dialog-label"
                aria-describedby="test_dialog-description">
                <div className="mdc-dialog__surface">
                    <header className="mdc-dialog__header">
                        <h2 id="test_dialog-label" className="mdc-dialog__header__title">Test Dialog</h2>
                    </header>
                    <section className="mdc-dialog__body">
                    </section>
                    <footer className="mdc-dialog__footer">
                        <button onClick={() => this.dialog.close()}>CLOSE</button>
                    </footer>
                </div>
                <div className="mdc-dialog__backdrop"></div>
            </aside>
        );
    }
}

export default Dialog;