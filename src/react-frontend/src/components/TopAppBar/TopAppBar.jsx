import React, { Component } from "react";
import "./TopAppBar.scss";
import {MDCTopAppBar} from '@material/top-app-bar';
import NavTabBar from "./components/NavTabBar/NavTabBar.jsx";
import Toolbar from "./components/Toolbar/Toolbar.jsx";
import  { NavLink }  from 'react-router-dom';

class TopAppBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topAppBar: null,
        };
    }

    componentDidMount() {
        this.setState({
            topAppBar: new MDCTopAppBar(this.mdcMount),
        });
    }

    componentWillUnmount() {
        this.state.topAppBar.destroy();
    }

    render() {
        return (
            <div id="headerContainer">
                <header className="mdc-top-app-bar mdc-top-app-bar--fixed" ref={(header) => {this.mdcMount = header}}>
                    <div className="mdc-top-app-bar__row">
                        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                            {/*<button className="material-icons mdc-top-app-bar__navigation-icon mdc-icon-button">menu</button>*/}
                            <NavLink exact to="/" className="app-title-navlink">
                                <span className="mdc-top-app-bar__title">Smooshify</span>
                            </NavLink>
                        </section>
                        <NavTabBar></NavTabBar>
                        <Toolbar></Toolbar>
                    </div>
                </header>
                <div className="mdc-top-app-bar--fixed-adjust">
                </div>
            </div>
        );
    }
}
export default TopAppBar;
