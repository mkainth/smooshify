import React, { Component } from 'react';
import {MDCMenu} from '@material/menu';
import './Toolbar.scss';

class Toolbar extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
        this.menu = new MDCMenu(this.mdcMount);
        this.menu.open = true;
    }

    componentWillUnmount() {
        this.state.menu.destroy();
    }

    render() {
      return (
        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-end" role="toolbar">
            <button className="material-icons mdc-top-app-bar__action-item mdc-icon-button" aria-label="Bookmark this page">bookmark</button>
            <button className="material-icons mdc-top-app-bar__action-item mdc-icon-button" aria-label="Share this page">share</button>
            <div id="moreButton" className="mdc-menu-surface--anchor">
                <button className="material-icons mdc-top-app-bar__action-item mdc-icon-button" aria-label="More">more_vert</button>
                <div className="mdc-menu mdc-menu-surface" ref={(div) => { this.mdcMount = div }}>
                    <ul className="mdc-list" role="menu" aria-hidden="true" aria-orientation="vertical" tabIndex="-1">
                        <li>
                        <ul className="mdc-menu__selection-group">
                            <li className="mdc-list-item" role="menuitem">
                            <span className="mdc-list-item__graphic mdc-menu__selection-group-icon">
                                ...
                            </span>
                            <span className="mdc-list-item__text">Single</span>
                            </li>
                            <li className="mdc-list-item" role="menuitem">
                            <span className="mdc-list-item__graphic mdc-menu__selection-group-icon">
                            ...
                            </span>
                            <span className="mdc-list-item__text">1.15</span>
                            </li>
                        </ul>
                        </li>
                        <li className="mdc-list-divider" role="separator"></li>
                        <li className="mdc-list-item" role="menuitem">
                        <span className="mdc-list-item__text">Add space before paragraph</span>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
    );}
}

export default Toolbar;