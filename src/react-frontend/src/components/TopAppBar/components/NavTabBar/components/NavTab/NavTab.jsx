import React, { Component } from 'react';
import  { NavLink}  from 'react-router-dom';
import { __RouterContext as RouterContext, matchPath } from "react-router";
import './NavTab.scss';

class NavTab extends Component {
    constructor(props) {
        super(props);
    }

    render() {
      return (
        <NavLink exact={this.props.exact} to={this.props.to} activeClassName="mdc-tab--active" className="remove-text-decoration mdc-tab mdc-tab-indicator-surface" role="tab" aria-selected="true" tabIndex="0">
            <span className="mdc-tab__content">
                <span className="mdc-tab__icon material-icons" aria-hidden="true">{this.props.icon}</span>
                <span className="mdc-tab__text-label">{this.props.label}</span>
            </span>
            <RouterContext.Consumer>
                {context => {
                    if ((!this.props.exact && (context.location.pathname.startsWith(this.props.to))) || (this.props.exact && (context.location.pathname == this.props.to))) {
                        return (
                            <span className="mdc-tab-indicator mdc-tab-indicator--active">
                                <span className="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                            </span>
                        );
                    } else {
                        return (
                            <span className="mdc-tab-indicator">
                                <span className="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                            </span>
                        );
                    }
                }}
                
            </RouterContext.Consumer>

            <span className="mdc-tab__ripple"></span>
        </NavLink>
    );}
}

export default NavTab;