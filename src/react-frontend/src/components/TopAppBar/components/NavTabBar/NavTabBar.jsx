import React, { Component } from 'react';
import  { Link }  from 'react-router-dom';
import {MDCTabBar} from '@material/tab-bar';
import './NavTabBar.scss';
import NavTab from './components/NavTab/NavTab.jsx';

class NavTabBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabBar: null,
        };
    }

    componentDidMount() {
        this.setState({
            tabBar: new MDCTabBar(this.mdcMount),
        });
    }

    componentWillUnmount() {
        this.state.tabBar.destroy();
    }

    render() {
      return (
        <section className="mdc-top-app-bar__section mdc-top-app-bar__section--align-center" role="navigation">
            <div id="NavTabBar" className="mdc-tab-bar" role="tablist" ref={(div) => { this.mdcMount = div }}>
                <div className="mdc-tab-scroller">
                    <div className="mdc-tab-scroller__scroll-area">
                        <nav className="mdc-tab-scroller__scroll-content ">
                            <NavTab exact={true} icon="home" label="Home" to="/"></NavTab>
                            <NavTab icon="bar_chart" label="Analytics" to="/analytics"></NavTab>
                            <NavTab icon="build" label="Our Stack" to="/stack"></NavTab>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    );}
}

export default NavTabBar;