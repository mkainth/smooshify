import React, { Component } from "react";
import "./URLTextbox.scss";
import {MDCTextField} from '@material/textfield';

class URLTextbox extends Component {
	constructor() {
		super();
		this.state = {
			textField: null,
		};
	}

	componentDidMount() {
		this.textField = new MDCTextField(this.mdcMount);
	}

	componentWillUnmount() {
		this.textField.destroy();
	}

	render() {
		return (
			<div className="mdc-text-field" ref={(div) => {this.mdcMount = div}}>
				<input type="text" id="my-text-field" className="mdc-text-field__input"></input>
				<label className="mdc-floating-label" htmlFor="my-text-field">Hint text</label>
				<div className="mdc-line-ripple"></div>
			</div>
		);
	}
}
export default URLTextbox;