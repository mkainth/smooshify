import { ADD_ARTICLE } from "../constants/action-types.jsx";
export function addArticle(payload) {
    return { type: ADD_ARTICLE, payload };
}