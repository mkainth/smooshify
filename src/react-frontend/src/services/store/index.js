import { createStore } from "redux";
import rootReducer from "./reducers/root.jsx";

const store = createStore(rootReducer);

export default store;