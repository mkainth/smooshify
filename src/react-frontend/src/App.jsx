import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Switch, Route, Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import Home from "./scenes/Home/Home.jsx";
import Analytics from "./scenes/Analytics/Analytics.jsx";
import "./App.scss";
import TopAppBar from "./components/TopAppBar/TopAppBar.jsx";
import { Provider } from "react-redux";
import store from "./services/store/";

const history = createBrowserHistory();
class App extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <Router history={ history }>
                <div className="App dark-mode"> {/*dark-mode*/}
                    <TopAppBar history={ history }>
                    </TopAppBar>
                    <div id="body">
                        <Switch>
                            <Route exact path="/" component={ Home } />
                            <Route path="/analytics" component={ Analytics } />
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

const wrapper = document.getElementById("App");
wrapper ? ReactDOM.render(<Provider store={store}><App /></Provider>, wrapper) : false;