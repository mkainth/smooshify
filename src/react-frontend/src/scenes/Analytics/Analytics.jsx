import React, { Component } from "react";
import "./Analytics.scss";
import URLTextbox from "../../components/URLTextbox/URLTextbox.jsx";
import ShareButton from "./components/ShareButton/ShareButton.jsx";

class Analytics extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div id="AnalyticsPage">
                <ShareButton></ShareButton>
            </div>
        );
    }
}

export default Analytics;