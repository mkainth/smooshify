import React, { useState } from 'react';
import "./ShareButton.scss";

export default function ShareButton(props) {

    return (
        <div id="fab_ctn" className="mdl-button--fab_flinger-container">
            <button id="fab_btn" className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
                <i className="material-icons">add</i>
            </button>
            <div className="mdl-button--fab_flinger-options">
                <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                <i className="material-icons">local_printshop</i>
                </button>
                <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                <i className="material-icons">content_copy</i>
                </button>
                <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                <i className="material-icons">content_paste</i>
                </button>
            </div>
        </div>
    );
}