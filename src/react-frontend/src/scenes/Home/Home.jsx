import React, { Component } from "react";
import "./Home.scss";
import "./components/CreateButton/CreateButton.jsx";
import CreateButton from "./components/CreateButton/CreateButton.jsx";

class Home extends Component {
    constructor() {
        super();
    }

  render() {
        return (
            <div id="HomePage">
                <CreateButton></CreateButton>
            </div>
        );
    }
}
export default Home;