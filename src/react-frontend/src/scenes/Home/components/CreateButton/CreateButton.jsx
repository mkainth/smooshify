import React, { useState } from 'react';
import "./CreateButton.scss";
import {MDCRipple} from '@material/ripple';

export default function CreateButton(props) {

    return (
        <button className="mdc-fab create-button-fab" aria-label="Favorite">
            <span className="mdc-fab__icon material-icons">add</span>
        </button>
    );
}