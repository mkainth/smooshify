# Smooshify

Smooshify is a cloud-enabled, mobile-ready short-url service. Smooshify allows users to shorten urls and view analytics on the performance of their short-url links.

### Stack
  - Front-End: Reach, Material Design Lite
  - Back-End: GoLang
  - Database: MongoDB
  - Infrastructure: AWS EC2, Route53

### Current Design
  - We have a Go server that serves static React files and serves endpoints for url generation and analytics.
  - I chose to use Material Design Lite for its simplicity and clean design
  
### Data Model

```go

  type URLMapping struct {
    ShortID   string              `json:"shortid" bson:"shortid"`
    LongURL   string              `json:"longurl" bson:"longurl"`
    CreatedOn string              `json:"createdon" bson:"createdon"`
    People    []Person            `json:"people" bson:"people"`
    Visits    int                 `json:"visits" bson:"visits"`
  }
```
  
  - We generate our own Short URL ID (ShortID) using a slightly shorter version of the Mongo Object ID algorithm (Ref: https://godoc.org/github.com/rs/xid)
  - We store the original url given to us (LongURL) along with a timestamp of the creation time
  - "People" is a record of IP addresses and timestamps. This is meant to capture who visited the Short URL (and when it was accessed).
  - "Visits" is the number of times the Short URL was visited.

### Todos
  - Separate the front-end and back-end completely by hosting the static React on AWS S3 and create Lambda endpoints for url generation and analytics. (Better Scalability + Low Cost)
  - Allow for concurrent DB Sessions to be opened
  - Better front-end error handling (new components)
  - Modularize & Integrate test cases (Ref: https://medium.com/@eminetto/clean-architecture-using-golang-b63587aa5e3f)
  - Configure Jenkins for CI
  - Look into a shorter url generation algorithm (Idea: format-preserving encryption algorithm that grows in length as more ids get produced)
 