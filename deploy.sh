#!/bin/bash

scp -i /home/michael/Downloads/Smooshify.pem /home/michael/go/src/smooshify/src/app/app ec2-user@ec2-35-182-148-128.ca-central-1.compute.amazonaws.com:smooshify/server/app
scp -i /home/michael/Downloads/Smooshify.pem -r /home/michael/go/src/smooshify/src/client/build ec2-user@ec2-35-182-148-128.ca-central-1.compute.amazonaws.com:smooshify/server/app